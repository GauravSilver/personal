const express = require("express");
const app = express();
const router = express.Router();
global.__basedir = __dirname + "/";

const dotenv = require('dotenv')
const morgan = require('morgan')
const cors = require('cors')
const bodyParser = require('body-parser');
var dbSequelize = require('./models/psql') ;
const passport = require('passport');
var session = require('express-session')
// const passport = require('passport');

dotenv.config();


const NODE_ENV = (process.env.NODE_ENV === 'production') ? 'production' : 'development'

const APPNAME = process.env.APP_NAME || 'Node'
const APP_ENV = process.env.APP_ENV || 'development'
const HOST = process.env.APP_URL || '127.0.0.1'
const PORT = process.env.APP_PORT || 7001

app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: true }));

// app.use(morgan('dev'))
// app.use(compression())
// app.use(helmet())

// app.use(passport.initialize());
// app.use(passport.session());
// app.use(cors());

app.get('/', (req, res) => {
    if (APP_ENV == 'development') {
        // log(req)
    }
    res.send({
        success: true,
        subject: 'Test Response',
        message: `Welcome to ${APPNAME} , Application Environment ${APP_ENV}`
    })
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept , Authorization");
    res.header( "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS")
    if ("OPTIONS" === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
});

//  Connect all our routes to our application
require('./routes/routes')(router);
app.use("/api/v1", router);


app.listen(PORT, HOST, (err, res) => {
    if (err) {
        console.log(`\n Server error on http://${HOST}:${PORT}/ \n`)
    } else {
        console.log('\n Up and running --- This is our CSV.')

        console.log('\n/*\n|--------------------------------------------------------------------------')
        console.log(`| Server up \n| Running on http://${HOST}:${PORT}/`)
        console.log('|--------------------------------------------------------------------------\n*/\n')

        console.log('+--------------------+-------------------------+--------------------------+')
        console.log('|    App Name          Environment        App Start Time                  |')
        console.log('+--------------------+-------------------------+--------------------------+')
        console.log('|                                                                         |')
        console.log(`|  ${APPNAME}        ${APP_ENV.toUpperCase()}      ${new Date().toLocaleString()}`)
        console.log('|                                                                         |')
        console.log('+--------------------+-------------------------+--------------------------+\n')

        dbSequelize.sequelize.authenticate().then(function (err) {
            console.log('\n Database Connection has been established successfully.\n');
        }).catch(function (err) {
            console.log('\nUnable to connect to the database:', err, '\n');
        });

    }
})

// function log(req) {
//     console.log('--> req.path', req.path)
//     // console.log('--> req.baseUrl', req.baseUrl)
//     // console.log('--> req.app.parent', req.app.parent)
//     // console.log('--> req.app.mountpath', req.app.mountpath)
//     console.log('--> req.app.path()', req.app.path())
// }
