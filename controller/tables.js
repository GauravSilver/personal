const pgp = require('pg-promise')({});
const { sequelize } = require('../models/psql');


const connection = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
}

const db = pgp(connection);
const table = {}; 


table.insertCSV = (tableName,columns,data) => {
    // let {tablename, columns, values  } = queryData;
    console.log("tableName", tableName)
    console.log("columns", columns)
    console.log("data", data)
    let query = `INSERT INTO ${tableName} (${columns}) VALUES (${data}) ;`
    console.log('query', query)
    return db.any(query);

}



module.exports = table;

