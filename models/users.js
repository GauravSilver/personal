module.exports = (sequelize, DataTypes) => {
    let user = sequelize.define('user', {
        name: {
            type: DataTypes.STRING(60),
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        phone: {
            type: DataTypes.STRING(20),
            allowNull: true,
        },
        password: {
            type: DataTypes.TEXT,
            allowNull: true,
        }
        
    },{
        tableName: "user",
        timestamps: false
    });
    console.log("user------>",user)
    return user;
}