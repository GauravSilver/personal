module.exports = (sequelize, DataTypes) => {
    let user_admin = sequelize.define('user_admin', {
        name: {
            type: DataTypes.STRING(60),
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        phone: {
            type: DataTypes.INTEGER(20),
            allowNull: true,
        },
        password: {
            type: DataTypes.TEXT,
            allowNull: true,
        }
        
    },{
        tableName: "user_admin",
        timestamps: false
    });
    return user_admin;
}