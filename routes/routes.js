const cronCron = require('../cron/cron');
const upload = require("../middleware/upload");
const usrCtrl = require('../controller/user')

let routes = (app) => {
    app.get('/getCron', cronCron.schedule);
    app.post('/uploadCSV', upload.single("file"), usrCtrl.uploadCSV);

  }

module.exports = routes;

