const multer = require('multer');
const path = require ('path')
const _basedir = path.resolve();
const csvFilter = (req, file, cb) => {
    console.log('*****csvFilter*****');
    console.log("_basedir", _basedir);

    if (file.mimetype.includes('csv')) {
        cb(null, true)
    } else {
        cb('Please upload csv file only', false);
    }
};

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, _basedir + "/uploads/");
    },
    
    filename: (req, file, cb) => {
        console.log('--->filename', file.originalname);
        cb(null,`${Date.now()}-coupons-${file.originalname}`);
    },
});


var uploadFile = multer({ storage: storage, fileFilter: csvFilter})
module.exports = uploadFile;




