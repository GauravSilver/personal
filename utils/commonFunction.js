'use strict';
var statusCodeMsg = require('./statusCodeMsg'),
	sequelize = require('sequelize');

module.exports = {

	sendResponse: function (codeRes, statusRes, resultRes, cb) {
		if (statusRes) {
			cb({
				code: codeRes,
				message: statusCodeMsg[codeRes],
				status: statusRes,
				results: resultRes
			});
		} else {
			cb({
				code: codeRes,
				message: statusCodeMsg[codeRes],
				status: statusRes
			})
		}
	},

	dateFormatChange: function (d) {
		var yearString = d.getFullYear().toString();
		var monthString = d.getMonth() + 1;
		var dateString = d.getDate().toString();
		var hour = d.getHours().toString();

		if (monthString < 10) {
			monthString = '0' + monthString;
		}

		if (dateString.length == 1) {
			dateString = '0' + dateString;
		}
		var d1 = yearString + '-' + monthString + '-' + dateString;
		return d1;
	}

};
